require_relative 'string_to_phone'
require 'test/unit'

class StringToPhoneTest < Test::Unit::TestCase
  def test_case_1
    input = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    assert_equal "(123) 456-7890", StringToPhone.new(input).execute
  end

  def test_case_2
    input = [1, 2, 30, 4, 5, 6, 7, 8]
    assert_raises Exception.new "Not long enough" do
      StringToPhone.new(input).execute
    end
  end

  def test_case_3
    input = "Foo"
    assert_raises Exception.new "Not an Array" do
      StringToPhone.new(input).execute
    end
  end

  def test_case_4
    input = [9, 9, 9, 1, 2, 1, 0, 7, 2, 4]
    assert_equal "(999) 121-0724", StringToPhone.new(input).execute
  end
end
