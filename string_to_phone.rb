class StringToPhone
  attr_reader :input

  def initialize(input)
    @input = input
  end

  def execute
    raise Exception.new "Not an Array" unless input.is_a?(Array)

    filter_numbers
    raise Exception.new "Not long enough" unless input.length == 10

    add_symbols
  end

  private

  def filter_numbers
    input.filter! do |number|
      number >= 0 && number <= 9
    end
  end

  def add_symbols
    input.join.insert(0, "(").insert(4, ") ").insert(9, "-")
  end
end
